<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Film Page</title>
        <link href="/bootstrap-5.3.3-dist/css/bootstrap.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.10.2/umd/popper.min.js"></script>
    </head>
    <body style="color:#EEEEEE; background-color: #101827;">

        <nav style="background-color:#1F2937">
            <div class="container">
                <ul class="nav nav-pills">
                    <a href="/dashboard"><img src="https://img.freepik.com/premium-vector/javanese-hat-symbol-blangkon_757387-2379.jpg" style="width: 45px; height: 45px; border-radius: 50%; margin-right: 10px;"></a>
                    <li class="nav-item dropdown" style="margin-left: auto">
                        <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">{{ Auth::user()->name }}</a>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="/profile">Profile</a></li>
                            <li><hr class="dropdown-divider"></li>
                            <li><a class="dropdown-item" href="{{ route('logout') }}">Logout</a></li>
                        </ul>
                    </li>
                
                </ul>
                
            </div>
        </nav>

        <div class="container" style="padding-top:30px">
            <!-- section awal -->
            <div style="display: flex">
                <!-- video -->
                <iframe width="800" height="450" src="{{ $film-> trailer}}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
            
                <!-- Title etc -->
                <div class="information">
                    <h3>{{ $film->title}}</h3>
                    <p>
                        {{ $film->release_year}}, {{ $film->genre}}, {{ $film->duration}} menit
                    </p>
                    <br>

                    <div style="display: flex; justify-content: center; align-items: center;">
                        <img src="https://img.freepik.com/premium-vector/javanese-hat-symbol-blangkon_757387-2379.jpg" style="width: 80px; height: 80px; border-radius: 50%; margin-right: 10px;"> 
                        <h3>{{$average_rating}} / 5.0</h3>
                    </div>
                    <p>AUDIENCE SCORE</p><br>

                    <p style="font-size:10px; font-style: italic;">
                        The audience score only displays the average score assessment of all people. <br>
                        <span style="color: #DD5746;">Don't get too hung up on people's judgments.</span>
                    </p>

                    
                </div>
            </div>
            
            <div class="movie-information" style="color:#EEEEEE; background-color: #101827">
                <h3><span style="color:#FFC94A">|</span> Movie Information</h3>
                <p>{{ $film->synopsis}}</p>

                <table>
                    <tr>
                        <th>Director </th>
                        <td>:  {{ $film->director}}</td>
                    </tr>
                    <tr>
                        <th>Genre </th>
                        <td>:  {{ $film->genre}}</td>
                    </tr>
                    <tr>
                        <th>Release Year </th>
                        <td>:  {{ $film->release_year}}</td>
                    </tr>
                    <tr>
                        <th>Duration </th>
                        <td>:  {{ $film->duration}} minute</td>
                    </tr>
                    <tr>
                        <th>Rating </th>
                        <td>:  {{ $film->rating}}</td>
                    </tr>
                </table>

            </div>
        
            <!-- casting -->
            <div class="cast-information">
                <h3><span style="color:#FFC94A">|</span> Cast</h3>
                <p>The actors who took part in the film making process</p>
                <div class="row">
                    @foreach ($pemerans as $pemeran)
                        <div class="casting-card">
                            <img class="casting" src="{{ $pemeran->url_photo }}" alt="{{ $film->title }}">
                            <div>
                                <h6 class="card-title"><b>{{ $pemeran->name }}</b></h6>
                                <p class="card-text">{{ $pemeran->casting }}</p>
                            </div>
                        </div> 
                    @endforeach
                </div>
            </div>

            <!-- review -->
            <div class="review-information">
                <h3><span style="color:#FFC94A">|</span> Reviews</h3>
                <p>Find out what other people think about this film</p>

                    @foreach ($reviews as $review)
                        <div class="review-card">
                            <table style="width: 100%">
                                <tr>
                                    <th>{{ $review->user_name }}</th>
                                    <td style="width: 120px">{{ $review->created_at}}</td>
                                </tr>
                                <tr>
                                    <td>{{ $review->review_text }}</td>
                                    <td style="width: 120px">
                                    @for ($i = 1; $i <= 5; $i++)
                                        <span class="star" style="color: rgb(226, 208, 53); font-size: 20px; {{ $i <= $review->rating ? '' : 'opacity: 0.3;' }}">&#9733;</span>
                                    @endfor
                                    </td>
                                </tr>
                            </table>
                        </div>

                    @endforeach

                <div class="comment">
                    <h3><span style="color:#FFC94A">|</span> Comments</h3>
                    <p>Publish your comment here so others know what you think!</p>

                    @if(session('comment_done'))
                        <p style="background-color:#A0153E; padding:10px; padding-left:20px; border-radius: 15px;">{{ session('comment_done') }}</p>
                    @elseif(session('success'))
                        <p style="background-color:#9ADE7B; padding:10px; padding-left:20px; border-radius: 15px;">{{ session('success') }}</p>
                    @endif

                    <div class="comment-body">
                        <p>Username : {{ Auth::user()->name }}</p>
                        
                        <div>
                            <form method="POST" action="{{ route('post.comment') }}">

                                @csrf
                                <input type="hidden" name="film_id" value="{{ $film->id }}">
                                <div class="form-floating">
                                    <textarea class="form-control" name="review_text" placeholder="Leave a comment here" id="floatingTextarea2" style="height: 100px"></textarea>
                                    <label for="floatingTextarea2">Comments</label>
                                </div>

                                <!-- rating -->
                                <div class="rating-wrapper">
                                    <input type="radio" name="rating" id="star-5" value="5"><label for="star-5"></label>
                                    <input type="radio" name="rating" id="star-4" value="4"><label for="star-4"></label>
                                    <input type="radio" name="rating" id="star-3" value="3"><label for="star-3"></label>
                                    <input type="radio" name="rating" id="star-2" value="2"><label for="star-2"></label>
                                    <input type="radio" name="rating" id="star-1" value="1"><label for="star-1"></label>
                                    <br><br><br>
                                    <p>~ Give a rating according to your opinion ~</p>
                                </div>
                                <!-- sampek sini -->

                                <div class="btn-comment" style="display: flex; justify-content: flex-end">
                                    <button type="submit" class="btn btn-secondary">Post Comment</button>
                                </div>
                        </div>
                        
                    </div>
                    
                </div>    
            </div>

            
            
        </div>
        <script src="/bootstrap-5.3.3-dist/js/bootstrap.js"></script>
    </body>
</html>
