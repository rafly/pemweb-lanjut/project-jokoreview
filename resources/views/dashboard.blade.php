<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Film Page</title>
        <link href="/bootstrap-5.3.3-dist/css/bootstrap.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.10.2/umd/popper.min.js"></script>
    </head>

    <body style="background-color: #101827">
        <nav style="background-color:#1F2937">
            <div class="container">
                <ul class="nav nav-pills">
                    <a href="/dashboard"><img src="https://img.freepik.com/premium-vector/javanese-hat-symbol-blangkon_757387-2379.jpg" style="width: 45px; height: 45px; border-radius: 50%; margin-right: 10px;"></a>
                    <li class="nav-item dropdown" style="margin-left: auto">
                        <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">{{ Auth::user()->name }}</a>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="/profile">Profile</a></li>
                            <li><hr class="dropdown-divider"></li>
                            <li><a class="dropdown-item" href="{{ route('logout') }}">Logout</a></li>
                        </ul>
                    </li>
                
                </ul>
                
            </div>
        </nav>

        
        <div class="container" style="padding-top: 30px">
        
            <!-- Top film -->
            <div class="top-rating" style="color:#ffffff; border-bottom: 1px solid #9e9e9e73; padding-bottom:50px;">
                <h3><span style="color:#FFC94A">|</span> Top Film by Rating</h3>
                <p>We present films with the best ratings based on reviews</p>
                
                <div style="display:flex">
                    @if($topRatedFilms->count() > 0)
                        <div style="margin:0px 20px 0px 0px">
                            <iframe width="800" height="450" src="{{$topRatedFilms[0]-> trailer}}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
                            <h2><b>{{ $topRatedFilms[0]->title }}</b></h2>
                            <p>
                                <b>Director :</b> {{ $topRatedFilms[0]->director }} <br>
                                <b>Release Year :</b> {{ $topRatedFilms[0]->release_year }} <br>
                                <b>Genre :</b> {{ $topRatedFilms[0]->genre }} <br>
                                <b>Rating :</b> {{ number_format($topRatedFilms[0]->avg_rating, 1) }} <br>
                            </p>
                        </div>
                    @endif

                    <div>
                        @if($topRatedFilms->count() > 1)
                            <div style="margin-bottom:20px">
                                <h4><span style="color:#FFC94A">|</span> Best <span style="color:#FFC94A">Second</span> Film by Rating</h4>
                                <div style="width: auto; background-color:#1F2937; color:#ffffff; display:flex; border-radius:5px">
                                    <a href="{{ route('detail', ['id' => $topRatedFilms[1]->id]) }}">
                                        <img src="{{ $topRatedFilms[1]->cover_image_url }}" alt="{{ $topRatedFilms[1]->title }}" style="width: 140px; height: 210px; padding:10px">
                                    </a>
                                    <div style="margin:20px 20px 20px 0px">
                                        <h5>{{ $topRatedFilms[1]->title }}</h5>
                                        <p style="font-size:12px; margin: 20px 0px 0px 20px">
                                            <b>Duration : </b>{{ $topRatedFilms[1]->duration }} mnt <br>
                                            <b>Genre : </b>{{ $topRatedFilms[1]->genre }} <br>
                                            <b>Rating : </b>{{ number_format($topRatedFilms[1]->avg_rating, 1) }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endif

                        @if($topRatedFilms->count() > 2)
                            <div>
                                <h4><span style="color:#FFC94A">|</span> Best <span style="color:#FFC94A">Third</span> Film by Rating</h4>
                                <div style="width: auto; background-color:#1F2937; color:#ffffff; display:flex; border-radius:5px">
                                    <a href="{{ route('detail', ['id' => $topRatedFilms[2]->id]) }}">
                                        <img src="{{ $topRatedFilms[2]->cover_image_url }}" alt="{{ $topRatedFilms[2]->title }}" style="width: 140px; height: 210px; padding:10px">
                                    </a>
                                    <div style="margin:20px 20px 20px 0px">
                                        <h5 class="card-title">{{ $topRatedFilms[2]->title }}</h5>
                                        <p class="card-text" style="font-size:12px; margin: 20px 0px 0px 20px">
                                            <b>Duration : </b>{{ $topRatedFilms[2]->duration }} mnt <br>
                                            <b>Genre : </b>{{ $topRatedFilms[2]->genre }} <br>
                                            <b>Rating : </b>{{ number_format($topRatedFilms[2]->avg_rating, 1) }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    
                </div>
            </div>

            <!-- Semua film -->
            <div class="film_all" style="color:#ffffff; padding:50px 0px;">
                <h3><span style="color:#FFC94A">|</span> All Films</h3>
                <p>See reviews of your favorite films</p>

                <div class="row row-cols-1 row-cols-md-6 g-4">
                @foreach ($films as $film)
                    <div>
                        <div class="card" style="width: 200px; background-color:#1F2937; color:#ffffff">
                            <a href="{{ route('detail', ['id' => $film->id]) }}" class="card-img-top">
                                <img src="{{ $film->cover_image_url }}" alt="{{ $film->title }}" style="width: 200px; height: 300px; padding:10px">
                            </a>
                            <div class="card-body">
                                <h5 class="card-title">{{ $film->title }}</h5>
                                <p class="card-text" style="font-size:12px">{{ $film->duration }} mnt • {{ $film->genre }}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            </div>
            
        </div>


        <script src="/bootstrap-5.3.3-dist/js/bootstrap.js"></script>

    </body>
</html>
