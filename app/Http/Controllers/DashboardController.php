<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Films;
use App\Models\Cast;
use App\Models\Reviews;
use App\Models\User;
use App\Models\Mahasiswa;

class DashboardController extends Controller
{
    public function index()
    {
        $films = Films::all();
        // Mengambil data film dengan rata-rata rating
        $films_rating = Films::select('films.*', \DB::raw('(SELECT AVG(rating) FROM reviews WHERE reviews.film_id = films.id) as avg_rating'))
        ->orderByDesc('avg_rating')
        ->get();

    // Mengambil tiga film dengan rata-rata rating tertinggi
    $topRatedFilms = $films_rating->take(3);

    return view('dashboard', [
        'films' => $films,
        'topRatedFilms' => $topRatedFilms
    ]);
    }

    public function detail($id)
    {
        $film = Films::findOrFail($id);
        $pemerans = Cast::where('id_film', $id)->get();
        $reviews = Reviews::where('film_id', $id)
            ->join('users', 'reviews.user_id', '=', 'users.id')
            ->select('reviews.*', 'users.name as user_name')
            ->get();

        // Menghitung nilai rata-rata dari rating
        $average_rating = round(Reviews::where('film_id', $id)->avg('rating'), 1);
        
        return view('detail', [
            'film' => $film,
            'pemerans' => $pemerans,
            'reviews' => $reviews,
            'average_rating' => $average_rating
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'review_text' => 'required|string',
            'film_id' => 'required',
            'rating' => 'required'
        ]);

        $id = Auth::user()->id;
        $filmId = $request->film_id;

        $existingReview = Reviews::where('user_id', $id)->where('film_id', $filmId)->first();

        if ($existingReview) {
            return redirect()->back()->with('comment_done', 'You have already reviewed this film. Cannot review again.');
        }


        $request->merge(['user_id' => $id]);
        $request->merge(['review_date' => now()]);

        // Simpan data review ke database
        Reviews::create($request->all());

        return redirect()->back()->with('success', 'Comment success to publish!');
    }
}
